package pw.bowser.spm.sample

import pw.bowser.spm.SPMPlugin
import pw.bowser.spm.manifest.YAMLManifest

/**
 * Sample plugin
 * Turned in to a jar plugin for testing
 */
class SamplePlugin extends SPMPlugin[YAMLManifest] {

  /**
   * Called when the plugin is loaded
   */
  override def pluginLoaded(manifest: YAMLManifest): Unit = throw new RuntimeException("Plugin Was Loaded!")
}
