package pw.bowser.spm.test

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, Matchers, FlatSpec}
import pw.bowser.spm.loaders.JarPluginLoader
import java.io.File
import pw.bowser.spm.ScalaPluginManager
import pw.bowser.spm.manifest.YAMLManifest

@RunWith(classOf[JUnitRunner])
class JarPluginLoaderTest extends FlatSpec with Matchers {

  "The JAR plugin loader" should "properly load classfiles from the JAR" in {
    // Please run the test from project root
    val pluginDir = new File("./src/test/resources")
    println(s"Plugin directory is $pluginDir")

    val jarManager = new JarPluginLoader(pluginDir, getClass.getClassLoader)
    ScalaPluginManager.registerLoader(jarManager)

    ScalaPluginManager.beforeLoad {manifest => println(s"Loading ${manifest.groupId}.${manifest.name}"); true}

    var error: Option[Throwable] = None

    ScalaPluginManager.loadPlugins {(t, pm, cl, pl) => error = Some(t)}

    error should not be(None)
    error.get.getMessage should be("Plugin Was Loaded!")
  }

  "The JAR plugin loader" should "load the manifest in its entirety" in {
    // Please run the test from project root
    val pluginDir = new File("./src/test/resources")
    println(s"Plugin directory is $pluginDir")

    val jarManager = new JarPluginLoader(pluginDir, getClass.getClassLoader)

    var manifest: Option[YAMLManifest] = None

    jarManager.loadAvailablePlugins {(t, pm, cl, pl) => manifest = Some(pm.asInstanceOf[YAMLManifest])}

    val ymanifest = manifest.get

    ymanifest.groupId    should be("pw.hysteria")
    ymanifest.name       should be("Example")
    ymanifest.version    should be("1.0")
    ymanifest.mainClass  should be("pw.bowser.spm.sample.SamplePlugin")
    ymanifest.dependencies.size should be(1)
    ymanifest.properties("property") should be("value")
  }

}
