package pw.bowser.spm.test

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import pw.bowser.spm.{VersionExpression, Version}

import pw.bowser.spm.VersionExpression.string2VersionExpression

@RunWith(classOf[JUnitRunner])
class VersionExpressionSpec extends FlatSpec with Matchers {

  it should "parse human-readable version expressions" in {
    val humanVersionExpression = ">=1.5.6"
    val expression = VersionExpression.parse(humanVersionExpression)

    expression.operand should be(VersionExpression.CompatibilityOperand.GREATER_THAN_OR_EQUAL)
    expression.version should be(Version.parse("1.5.6"))
  }

  it should "evaluate compatibility of versions" in {
    val version           = Version("1.2")
    val versionExpression = VersionExpression.parse(">1.1")

    versionExpression(version) should be(true)
  }

  it should "infer the equals operand when no operand is present" in {
    val expression = VersionExpression.parse("1.2")

    expression.operand should be(VersionExpression.CompatibilityOperand.EQUAL)
    expression.version should be(Version.parse("1.2"))
  }

  "A string" should "coerce to a version expression" in {
    ">1.2".isCompatibleWith(Version.parse("1.2.3"))
  }

}
