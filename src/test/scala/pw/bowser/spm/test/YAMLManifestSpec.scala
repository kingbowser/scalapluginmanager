package pw.bowser.spm.test

import org.scalatest.{FlatSpec, Matchers}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import scala.io.Source
import pw.bowser.spm.manifest.YAMLManifest
import pw.bowser.spm.VersionExpression

@RunWith(classOf[JUnitRunner])
class YAMLManifestSpec extends FlatSpec with Matchers {

  it should "load data from YAML without error" in {
    val yaml = Source.fromInputStream(getClass.getResourceAsStream("/plugin_manifest.yml")).getLines().mkString("\n")
    yaml.length > 0 should be(true)
    YAMLManifest.parse(yaml) should not be(null)
  }

  it should "construct from YAML" in {
    val yaml = Source.fromInputStream(getClass.getResourceAsStream("/plugin_manifest.yml")).getLines().mkString("\n")
    val manifest = YAMLManifest.parse(yaml)

    manifest.groupId    should be("pw.hysteria")
    manifest.name       should be("Example")
    manifest.version    should be("1.0")
    manifest.mainClass  should be("pw.bowser.spm.sample.SamplePlugin")

    manifest.dependencies(0)._1 should equal("pw.hysteria")
    manifest.dependencies(0)._2 should equal("Example2")
    manifest.dependencies(0)._3 should equal(VersionExpression.parse("1.0"))
  }

}
