package pw.bowser.spm.test

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import pw.bowser.spm.Version
import pw.bowser.spm.Version.string2Version

@RunWith(classOf[JUnitRunner])
class VersionSpec extends FlatSpec with Matchers {

  it should "be easily created from a human version" in {
    val hVersion  = "1.2.3b1a2"

    val version   = Version.parse(hVersion)
    version.extraVersion should be ("b1a2")
    version.encodedVersion should be (Seq(1, 2, 3))
  }

  it should "turn back in to human form" in {
    val hVersion  = "1.2.3b1a2"
    val version   = Version.parse(hVersion)

    version.humanForm should be(hVersion)
  }

  it should "be greater than another version" in {
    val version1  = Version.parse("1.2.2")
    val version2  = Version.parse("1.2.1")
    val version3  = Version.parse("1.2.3")

    version1 > version1 should be(false)
    version1 > version2 should be(true)
    version1 > version3 should be(false)
  }

  it should "be greater than or equal to another version" in {
    val version1  = Version.parse("1.2.2")
    val version2  = Version.parse("1.2.1")
    val version3  = Version.parse("1.2.3")

    version1 >= version1 should be(true)
    version1 >= version2 should be(true)
    version1 >= version3 should be(false)
  }

  it should "be equal to another version" in {
    val version1  = Version.parse("1.2.3")
    val version2  = Version.parse("1.2.2")
    val version3  = Version.parse("1.2.4")
    val version4  = Version.parse("1.2.3.4")

    version1 == version1 should be(true)
    version1 == version2 should be(false)
    version1 == version3 should be(false)
    version1 == version4 should be(false)
  }

  it should "reside in or be a superset of another revision" in {
    val version1  = Version.parse("1.2.3")
    val version2  = Version.parse("1.2.2")
    val version3  = Version.parse("1.2.4")
    val version4  = Version.parse("1.2.3.4")

    version1 >> version1 should be(true)
    version1 >> version2 should be(false)
    version1 >> version3 should be(false)
    version1 >> version4 should be(true)
  }

  it should "be less than another revision" in {
    val version1  = Version.parse("1.2.3")
    val version2  = Version.parse("1.2.2")
    val version3  = Version.parse("1.2.4")

    version1 < version1 should be(false)
    version1 < version2 should be(false)
    version1 < version3 should be(true)
  }

  it should "be less than or equal to another revision" in {
    val version1  = Version.parse("1.2.3")
    val version2  = Version.parse("1.2.2")
    val version3  = Version.parse("1.2.4")

    version1 <= version1 should be(true)
    version1 <= version2 should be(false)
    version1 <= version3 should be(true)
  }

  "A string" should "coerce to a version" in {
    Version.parse("1.2.3") isEqualTo "1.2.3" should be(true)
  }

}
