package pw.bowser.spm

/**
 * Plugin manifest contract
 *
 * Specifies parameters about the plugin that is being loaded
 *
 * @author Bowser
 */
trait PluginManifest {

  /**
   * Group that created the plugin
   *
   * @return creator id (group ID)
   */
  def groupId:  String

  /**
   * Name of the plugin
   *
   * @return plugin name
   */
  def name:     String

  /**
   * Plugin version
   *
   * @return version
   */
  def version:  String

  /**
   * Fully qualified name of plugin main class
   *
   * @return main class
   */
  def mainClass: String

  /**
   * Dependencies in the format of
   * (groupId, name, `VersionExpression`)
   *
   * Version expressions are a bit like gem version expression.
   * <pre>
   *   1.0 only: '1.0'
   *   any revision of 1.0: '1.*'
   *   1.0 or newer:
   * @return
   */
  def dependencies: List[(String, String, VersionExpression)]

}
