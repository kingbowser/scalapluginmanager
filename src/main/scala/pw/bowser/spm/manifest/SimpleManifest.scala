package pw.bowser.spm.manifest

import pw.bowser.spm.{PluginManifest, VersionExpression}

/**
 * Manifest created from constructor parameters
 */
class SimpleManifest(val groupId: String, val name: String, val version: String,
                     val dependencies: List[(String, String, VersionExpression)],
                     val mainClass: String) extends PluginManifest
