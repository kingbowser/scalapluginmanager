package pw.bowser.spm.manifest

import pw.bowser.spm.{VersionExpression, PluginManifest}
import scala.beans.BeanProperty

import collection.JavaConversions._
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

/**
 * Plugin manifest assembled from YAML data
 */
class YAMLManifest(beanData: YAMLManifest.BeanDataIntermediary) extends PluginManifest {

  val _depCache = beanData.dependencies

  /**
   * Group that created the plugin
   *
   * @return creator id (group ID)
   */
  override def groupId: String = beanData.groupId

  /**
   * Name of the plugin
   *
   * @return plugin name
   */
  override def name: String = beanData.name

  /**
   * Dependencies in the format of
   * (groupId, name, `VersionExpression`)
   *
   * Version expressions are a bit like gem version expression.
   * <pre>
   * 1.0 only: '1.0'
   * any revision of 1.0: '1.*'
   * 1.0 or newer:
   * @return
   */
  override def dependencies: List[(String, String, VersionExpression)] = _depCache

  /**
   * Plugin version
   *
   * @return version
   */
  override def version: String = beanData.version

  /**
   * Fully qualified name of plugin main class
   *
   * @return main class
   */
  override def mainClass: String = beanData.mainClass

  /**
   * Access manifest properties section
   *
   * @return properties
   */
  final def properties: Map[String, String] = beanData.properties.toMap
}

object YAMLManifest {

  final class BeanDataIntermediary {
    @BeanProperty var groupId: String   = null
    @BeanProperty var name: String      = null
    @BeanProperty var version: String   = null
    @BeanProperty var mainClass: String = null
    @BeanProperty var requires: java.util.ArrayList[java.util.Map[String, String]] = null
    @BeanProperty var properties: java.util.Map[String, String] = null

    def dependencies = if(requires != null) {
      requires.map {jm =>
        (jm.get("groupId"), jm.get("name"), VersionExpression.parse(jm.get("version")))
      }.toList
    } else {
      List[(String, String, VersionExpression)]()
    }
  }

  def parse(yaml: String): YAMLManifest = {
    val yamlLoader = new Yaml(new Constructor(classOf[BeanDataIntermediary]))

    new YAMLManifest(yamlLoader.load(yaml).asInstanceOf[BeanDataIntermediary])
  }

}
