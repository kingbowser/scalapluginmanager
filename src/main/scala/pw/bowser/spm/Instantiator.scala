package pw.bowser.spm

/**
 * Abstracts instantiation logic to allow for Injector implementation if needed
 */
trait Instantiator {

  /**
   * Call to create an instance of `clazz`
   * @param clazz       class
   * @param parameters  parameters (if applicable, implementation may not use these)
   * @tparam T          object type (inferred from Clazz)
   * @return            an instance of clazz
   */
  def instantiate[T](clazz: Class[T], parameters: AnyRef*): T

}
