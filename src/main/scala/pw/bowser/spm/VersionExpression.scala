package pw.bowser.spm

import scala.collection.mutable
import pw.bowser.spm.VersionExpression.CompatibilityOperand

/**
 * Represents a version compatibility expression (like >1.5)
 */
final class VersionExpression(val operand: CompatibilityOperand, val version: Version) {

  /**
   * Evaluate version compatibility
   * @param otherVersion other version
   * @return version
   */
  def isCompatibleWith(otherVersion: Version): Boolean = operand(otherVersion, version)

  /**
   * @see isCompatibleWith
   */
  def apply(otherVersion: Version): Boolean = isCompatibleWith(otherVersion)

  override def toString: String = s"VersionExpression(${operand.operand}, ${version.humanForm})"

  override def equals(other: Any): Boolean = other match {
    case ve: VersionExpression =>
      ve.version == version && ve.operand == operand
    case _ => false
  }

}

object VersionExpression {

  /**
   * Logic wrapper for compatibility expressions, like ">=1.4"
   * Any created operands are registered automatically.
   *
   * @param operand operator (like >=)
   * @param comparisonFunction logic used to evaluate operator
   */
  case class CompatibilityOperand(operand: String, comparisonFunction: (Version, Version) => Boolean) {

    CompatibilityOperand.operandMap.put(operand, this)

    /**
     * Evaluate the compatibility of two versions using the underlying compatibility function
     *
     * @param version1 first version
     * @param version2 second version
     * @return compatibility
     */
    def apply(version1: Version, version2: Version): Boolean = comparisonFunction(version1, version2)

  }

  object CompatibilityOperand {

    private val operandMap = mutable.Map[String, CompatibilityOperand]()

    def withName(name: String): CompatibilityOperand = operandMap(name)

    val GREATER_THAN          = CompatibilityOperand(">",  {case(v1, v2) => v1 >   v2})
    val GREATER_THAN_OR_EQUAL = CompatibilityOperand(">=", {case(v1, v2) => v1 >=  v2})
    val SUPERSET              = CompatibilityOperand(">>", {case(v1, v2) => v1 >>  v2})
    val EQUAL                 = CompatibilityOperand("=",  {case(v1, v2) => v1 ==  v2})
    val LESS_THAN             = CompatibilityOperand("<",  {case(v1, v2) => v1 <   v2})
    val LESS_THAN_OR_EQUAL    = CompatibilityOperand("<=", {case(v1, v2) => v1 <=  v2})
  }

  val EXPRESSION_REGEX = """(\D*)(\d(?:[\d\.]*\d)?(?:[\w\d]*)?)""".r("operand", "version")

  def apply(operand: String, version: Version)= new VersionExpression(CompatibilityOperand.withName(operand), version)

  /**
   * Create a VersionExpression
   * @param expression
   * @return
   */
  def parse(expression: String): VersionExpression = {
    val matcher = EXPRESSION_REGEX.pattern.matcher(expression)
    matcher.find()

    val (operand, hVersion) = (if(matcher.group(1).length > 0) matcher.group(1) else CompatibilityOperand.EQUAL.operand, matcher.group(2))

    VersionExpression(operand, Version.parse(hVersion))
  }

  implicit def string2VersionExpression(hExpression: String): VersionExpression = parse(hExpression)
}
