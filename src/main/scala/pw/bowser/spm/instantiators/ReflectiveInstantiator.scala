package pw.bowser.spm.instantiators

import pw.bowser.spm.Instantiator
import java.lang.reflect.Constructor

/**
 * Use vanilla java reflection to instantiate T
 */
final class ReflectiveInstantiator extends Instantiator {

  /**
   * Call to create an instance of `clazz`
   * @param clazz       class
   * @param parameters  parameters (if applicable, implementation may not use these)
   * @tparam T          object type (inferred from Clazz)
   * @return            an instance of clazz
   */
  override def instantiate[T](clazz: Class[T], parameters: AnyRef*): T =
    Option(clazz.getConstructor(parameters.map(_.getClass):_*)) match {
      case None => throw new IllegalStateException(s"No constructor in ${clazz.getName} with signature ${parameters.map(_.getClass)}")
      case cOpt: Some[Constructor[T]] =>
        val constructor = cOpt.get
        constructor.newInstance(parameters:_*)
    }

}
