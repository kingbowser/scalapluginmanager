package pw.bowser.spm.loaders

import pw.bowser.spm.{Instantiator, SPMPlugin, PluginManifest, PluginLoader}
import java.io.{FilenameFilter, File}
import scala.collection.mutable
import pw.bowser.spm.manifest.YAMLManifest
import java.net.URLClassLoader
import scala.io.Source
import pw.bowser.spm.instantiators.ReflectiveInstantiator

/**
 * Jar plugin loader.
 * Dependencies not enforced.
 *
 * Date: 6/21/14
 * Time: 4:26 PM
 */
final class JarPluginLoader(watchDirectory: File, parentClassLoader: ClassLoader,
                            instantiator: Instantiator = new ReflectiveInstantiator) extends PluginLoader {

  /**
   * Data structure containing loadde
   */
  private val loadedPlugins = mutable.Map[(String, String), (YAMLManifest, URLClassLoader, SPMPlugin[_])]()

  /**
   * Get a list of manifests representing plugins that this loader has loaded in to the virtual machine
   *
   * @return plugin manifests
   */
  override def plugins: List[(PluginManifest, SPMPlugin[_])] = loadedPlugins.values.map {case(y, u, p) => (y, p)}.toList

  /**
   * Get the ClassLoader used to load plugins
   *
   * @return ClassLoader
   */
  override def pluginClassLoader: ClassLoader = parentClassLoader

  /**
   * The pluginLoader should search for and load available plugins when this is called
   */
  override def loadAvailablePlugins(errorCallback: ((Throwable, PluginManifest, ClassLoader, SPMPlugin[_]) => Unit) = {(w, x, y, z) => Unit}): Unit = {
    Option(watchDirectory.listFiles(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = name.matches(""".*\.jar""")
    })) match {
      case candidates: Some[Array[File]] =>
        candidates.get.foreach(loadPlugin(errorCallback))
      case _ => // Ignore
    }
  }

  /**
   * Plugin loading logic
   * @param errorCallback called when an error occurs
   * @param path path to the jar containing the plugin
   * @return
   */
  private def loadPlugin(errorCallback: ((Throwable, PluginManifest, ClassLoader, SPMPlugin[_]) => Unit))(path: File): Unit = {

    val classLoaderForJarFile = new URLClassLoader(Array(path.toURI.toURL), pluginClassLoader)

    val rawYAML     = try {
      Source.fromInputStream(classLoaderForJarFile.getResourceAsStream("plugin_manifest.yml")).getLines().mkString("\n")
    } catch {
      case e: Throwable =>
        errorCallback(e, null, classLoaderForJarFile, null)
        return
    }

    val manifest    = YAMLManifest.parse(rawYAML)

    if(!canLoadPlugin(manifest)) return

    val pluginMain  = try {
      val clazz = classLoaderForJarFile.loadClass(manifest.mainClass)
      instantiator.instantiate(clazz).asInstanceOf[SPMPlugin[YAMLManifest]]
    } catch {
      case e: Throwable =>
        errorCallback(e, manifest, classLoaderForJarFile, null)
        return
    }

    try pluginMain.pluginLoaded(manifest) catch {case e: Throwable => errorCallback(e, manifest, classLoaderForJarFile, pluginMain)}
  }
}
