package pw.bowser.spm

/**
 * Represents a version and provides facilities to determine compatibility with other versions
 *
 * @param encodedVersion numeric sequence of shorts representing human-readable version (i.e. 1.2.3 is Seq(1, 2, 3))
 */
final case class Version(encodedVersion: Seq[Int], extraVersion: String) {

  /**
   * Return the human-readable version string
   *
   * Ex.
   *
   * {{
   *  Version(
   *    encodedVersion  = Seq(1, 2, 3),
   *    extraVersion    = "beta4"
   *  ).toHumanForm == "1.2.3beta4"
   * }}
   *
   * @return
   */
  def humanForm: String = s"${encodedVersion.mkString(".")}$extraVersion"

  /**
   * Test that this version is greater than another
   *
   * This does not take into account the extraVersion
   *
   * This comparison works by creating a sequence of tuples containing two truths: (Revision is greater than other, Revision is equal to other)
   *
   * For all but the first, it requires both truths be false in order to return false:
   * if the revision segment is less then its counterpart, it will continue.
   * if the revision segment is greater than its counterpart, it will return true immediately.
   * Iteration will continue if it is equal, but not greater.
   *
   * @param otherVersion other version
   * @return greater than otherVersion
   */
  def isGreaterThan(otherVersion: Version): Boolean = {
    val truths = encodedVersion.zip(otherVersion.encodedVersion).map {revPair => (revPair._1 > revPair._2, revPair._1 == revPair._2)}

    truths.dropRight(1).foreach {case(greater, equal) =>
      if(!greater && !equal) {
        return false
      } else if (greater) {
        return true
      }
    }

    truths.last._1
  }

  /**
   * Test that this version is greater than or equal to another
   *
   * This does not take in to account the extraVersion
   *
   * @param otherVersion other version
   * @return is greater than or equal to otherVersion
   */
  def isGreaterThanOrEqualTo(otherVersion: Version): Boolean = {
    val truths = encodedVersion.zip(otherVersion.encodedVersion).map {revPair => revPair._1 >= revPair._2}

    truths.foreach {truth =>
      if(!truth) return false
    }

    true
  }

  /**
   * Test that this version is the super-set of a more deep version
   *
   * This does not take in to account the extraVersion
   *
   * Ex.
   *
   * 1.2.4    isSameFamily 1.2.4.7.83.2 is true
   * 1.2.4.3  isSameFamily 1.2.4        is true
   * 
   * In a more technical sense, this checks that one or the other version 
   * and checks that the longest is a sub-revision of the shortest (whichever is the least complex (ie taking the fewest digit-sets)).
   *
   * @param otherVersion other version
   * @return is child of other or self
   */
  def isChild(otherVersion: Version): Boolean = {
    val truths = encodedVersion.zip(otherVersion.encodedVersion).map {revPair => revPair._1 == revPair._2}

    truths.forall(t => t)
  }

  /**
   * Test that this version is equal to another version
   *
   * This does not take in to account the extraVersion
   *
   * @param otherVersion other version
   * @return is equal to otherVersion
   */
  def isEqualTo(otherVersion: Version): Boolean = isChild(otherVersion) && otherVersion.encodedVersion.size == encodedVersion.size

  /**
   * Test that this version is less than another version
   *
   * This does not take in to account the extraVersion
   *
   * @param otherVersion other version
   * @return is less than otherVersion
   */
  def isLessThan(otherVersion: Version): Boolean = {
    val truths = encodedVersion.zip(otherVersion.encodedVersion).map {revPair => (revPair._1 < revPair._2, revPair._1 == revPair._2)}

    truths.dropRight(1).foreach {case(less, equal) =>
        if(!less && !equal) {
          return false
        } else if(less) {
          return true
        }
    }

    truths.last._1
  }

  /**
   * Test that this version is less than or equal to another version
   *
   * This does not take in to account the extraVersion
   *
   * @param otherVersion other version
   * @return is less than or equal to otherVersion
   */
  def isLessThanOrEqualTo(otherVersion: Version): Boolean = {
    val truths = encodedVersion.zip(otherVersion.encodedVersion).map {revPair => revPair._1 <= revPair._2}

    truths.foreach {truth =>
      if(!truth) return false
    }

    true
  }

  /**
   * @see isGreaterThan
   */
  def >(otherVersion: Version):   Boolean   = isGreaterThan(otherVersion)

  /**
   * @see isGreaterThanOrEqualTo
   */
  def >=(otherVersion: Version):  Boolean   = isGreaterThanOrEqualTo(otherVersion)

  /**
   * @see isEqualTo
   */
  def ==(otherVersion: Version):  Boolean   = isEqualTo(otherVersion)

  /**
   * @see isLessThan
   */
  def <(otherVersion: Version):   Boolean   = isLessThan(otherVersion)

  /**
   * @see isLessThanOrEqualTo
   */
  def <=(otherVersion: Version):  Boolean   = isLessThanOrEqualTo(otherVersion)

  /**
   * @see isChild
   */
  def >>(otherVersion: Version):  Boolean   = isChild(otherVersion)

}

object Version {

  private val VERSION_EXPRESSION = """(\d(?:[\d\.]*\d)?)([\w\d]*)?""".r("numericVersion", "extraVersion")

  def parse(humanForm: String): Version = {
    val matchData = VERSION_EXPRESSION.pattern.matcher(humanForm)

    matchData.find() // If it doesn't find anything, it will throw an exception - everyone is happy.

    val encoded   = matchData.group(1).split("\\.").map(_.toInt)

    new Version(encoded, matchData.group(2))
  }

  def apply(humanForm: String): Version = parse(humanForm)

  implicit def string2Version(hVersion: String): Version = parse(hVersion)

}
