package pw.bowser.spm

/**
 * This trait defines what are essentially callbacks for the plugin from the loader
 *
 * ALL IMPLEMENTATIONS MUST HAVE A NULLARY CONSTRUCTOR!
 *
 * @tparam M type of manifest to expect
 */
trait SPMPlugin[M <: PluginManifest] {

  /**
   * Called when the plugin is loaded
   */
  def pluginLoaded(manifest: M): Unit

}
