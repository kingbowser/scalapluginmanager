package pw.bowser.spm

import scala.collection.mutable
import java.net.URLClassLoader

/**
 * Handles plugin loading for its specific type of plugin.
 * Implementations are tasked with getting the manifest, and whatnot.
 *
 * @author Bowser
 */
trait PluginLoader {

  /*
   * Contract
   */

  /**
   * Get a list of manifests representing plugins that this loader has loaded in to the virtual machine
   *
   * @return plugin manifests
   */
  def plugins: List[(PluginManifest, SPMPlugin[_])]

  /**
   * Get the ClassLoader used to load plugins
   *
   * @return ClassLoader
   */
  def pluginClassLoader: ClassLoader

  /**
   * The pluginLoader should search for and load available plugins when this is called
   * @param errorCallback called when an error is caught during a plugin load
   */
  def loadAvailablePlugins(errorCallback: ((Throwable, PluginManifest, ClassLoader, SPMPlugin[_]) => Unit) = {(w, x, y, z) => Unit}): Unit

  /*
   * Logic
   */

  /**
   * Callbacks to be executed before a plugin loads
   */
  private var beforeCallbacks = mutable.MutableList[(PluginManifest) => Boolean]()

  /**
   * Add a function to be 'called back' when a plugin is loaded.
   * The function returns a boolean value - whether or not the plugin should be loaded.
   *
   * @param callback callback function
   */
  final def beforePluginLoaded(callback: (PluginManifest) => Boolean): Unit = {
    beforeCallbacks += callback
  }

  /**
   * This should be called in order to determine whether a plugin may be loaded or not
   *
   * @param manifest manifest
   * @return whether or not the plugin can be loaded
   */
  protected final def canLoadPlugin(manifest: PluginManifest): Boolean = beforeCallbacks.forall(_(manifest))

}
