package pw.bowser.spm

import scala.collection.mutable

/**
 * Manages different loaders and ties together sets of plugins
 *
 * @author Bowser
 */
object ScalaPluginManager {

  /**
   * Holds all the loaders
   */
  private var loaders = mutable.MutableList[PluginLoader]()

  /**
   * Holds all pre-load callbacks
   */
  private var beforeLoadCallbacks = mutable.MutableList[(PluginManifest) => Boolean]()

  /**
   * Get a list of all loaded plugins
   *
   * @return plugins
   */
  def loadedPlugins: List[PluginManifest] = loaders.toList.map(_.plugins.map(_._1)).flatten

  /**
   * Register a loader
   *
   * @param loader loader
   */
  def registerLoader(loader: PluginLoader): Unit = if(!loaders.contains(loader)) {
    loaders += loader
    loader.beforePluginLoaded {manifest => beforeLoadCallbacks.forall(_(manifest))}
  }

  /**
   * Register a pre-load callback for plugins
   * It may return false to cancel the load
   *
   * @param callback callback
   */
  def beforeLoad(callback: (PluginManifest) => Boolean): Unit = {
    beforeLoadCallbacks += callback
  }

  /**
   * Load all available plugins
   *
   * @param errorCallback error callback
   */
  def loadPlugins(errorCallback: ((Throwable, PluginManifest, ClassLoader, SPMPlugin[_]) => Unit) = {(w, x, y, z) => Unit}): Unit = loaders.foreach(_.loadAvailablePlugins(errorCallback))

}
