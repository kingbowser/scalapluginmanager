# Scala Plugin Manager (SPM)

Is a helper that makes it easy to load plugins of many types by allowing you to define workflows for different types
of plugins.

It also provides Handy Dandy™ callbacks and plugin manifest handling.

# Goals

This is a new project, and aims to provide

* Callbacks
* Abstracted plugin type handling during the path-to-classloader
* Easy way to manage the logistics of getting from the Point A to the ClassLoader
* Manifests
* FS Watching

# What it isn't

A plugin library

# What it is

SPM just provides logical helpers for managing loaded plugins, and gluing together systems of plugins

# Notes

Dependency handling logic (Version, compatibility expressions) is present, but not needed or required